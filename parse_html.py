#!/usr/bin/env python
import requests
from bs4 import BeautifulSoup as bs
from datetime import datetime
from rfeed import *

"""
<div class="job" id="1594465846001" style="margin-bottom: 8px;"><a class="jtitle" href="https://unjobs.org/vacancies/1594465147854" style="margin-top:0px;">Procurement Assistant, Geneva, Switzerland</a><br/>UNOG - United Nations Office at Geneva<br/>Updated: <time class="upd timeago" datetime="2020-07-11T11:10:46Z">2020-07-11T11:10:46Z</time> <br/><span id="j0"></span></div>
"""

url = 'https://unjobs.org/duty_stations/gva'

resp = requests.get(url)
soup = bs(resp.content, features='html.parser')
divs = soup.findAll('div', {'class': 'job'})

# Only get divs with jobs
job_divs = []
for div in divs:
    if div.get('id') != None:
        job_divs.append(div)

# Preparing for RSS
job_items = []
for job in job_divs:
    item = Item(
        title = job.find('a', {'class': 'jtitle'}).text,
        link = job.find('a', {'class': 'jtitle'}).get('href'),
        description = job.find('a', {'class': 'jtitle'}).text,
        author = job.br.next_sibling,
        guid = Guid(job.find('a', {'class': 'jtitle'}).get('href')),
            pubDate = datetime.strptime(job.find('time').text, '%Y-%m-%dT%H:%M:%SZ'))
    job_items.append(item)


# RSS feed
feed = Feed(
        title = 'UN jobs in Geneva',
        link = 'https://anubis.valiyev.com/unjobs/geneva.xml',
        description = 'UN jobs in Geneva',
        language = "en-US",
        lastBuildDate = datetime.now(),
        items = job_items)

print(feed.rss())
